<?php

namespace Drupal\widget_engine;

use Drupal\content_translation\ContentTranslationHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\widget_engine\Entity\WidgetType;

/**
 * Defines the translation handler for widgets.
 *
 * @ingroup entity_api
 */
class WidgetTranslationHandler extends ContentTranslationHandler {

  /**
   * {@inheritdoc}
   */
  protected function entityFormTitle(EntityInterface $entity) {
    return $this->t('<em>Edit @type</em> @title', [
      '@type' => WidgetType::load($entity->bundle())->label(),
      '@title' => $entity->label()
    ]);
  }

}
