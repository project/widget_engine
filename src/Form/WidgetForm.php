<?php

namespace Drupal\widget_engine\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\widget_engine\Entity\WidgetType;

/**
 * Form controller for Widget edit forms.
 *
 * @ingroup widget_engine
 */
class WidgetForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    /* @var $entity \Drupal\widget_engine\Entity\Widget */
    $form = parent::buildForm($form, $form_state);

    if ($this->operation == 'edit') {
      $form['#title'] = $this->t('<em>Edit @type</em> @title', [
        '@type' => WidgetType::load($this->entity->bundle())->label(),
        '@title' => $this->entity->label()
      ]);
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $entity = &$this->entity;

    $status = parent::save($form, $form_state);

    switch ($status) {
      case SAVED_NEW:
        $this->messenger()->addStatus($this->t('Created the %label Widget.', [
          '%label' => $entity->label(),
        ]));
        break;

      default:
        $this->messenger()->addStatus($this->t('Saved the %label Widget.', [
          '%label' => $entity->label(),
        ]));
    }
    $form_state->setRedirect('widget_engine.widget_preview_generate', ['widget' => $entity->id()]);
  }

}
