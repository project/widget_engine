<?php

namespace Drupal\widget_engine_domain_access\Plugin\EntityReferenceSelection;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Plugin\EntityReferenceSelection\DefaultSelection;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\domain\DomainNegotiatorInterface;
use Drupal\widget_engine_domain_access\WidgetEngineDomainAccessManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides specific access control for the widget entity type.
 *
 * @EntityReferenceSelection(
 *   id = "default:widget",
 *   label = @Translation("Widget selection"),
 *   entity_types = {"widget"},
 *   group = "default",
 *   weight = 1
 * )
 */
class WidgetSelection extends DefaultSelection {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Domain negotiator.
   *
   * @var \Drupal\domain\DomainNegotiatorInterface
   */
  protected $domainNegotiator;

  /**
   * Domain access manager.
   *
   * @var \Drupal\widget_engine_domain_access\WidgetEngineDomainAccessManager
   */
  protected $domainAccessManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
    AccountInterface $current_user,
    DomainNegotiatorInterface $domainNegotiator,
    WidgetEngineDomainAccessManager $domainAccessManager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $entity_type_manager,
      $module_handler,
      $current_user
    );

    $this->domainNegotiator = $domainNegotiator;
    $this->domainAccessManager = $domainAccessManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('module_handler'),
      $container->get('current_user'),
      $container->get('domain.negotiator'),
      $container->get('widget_engine_domain_access.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getReferenceableEntities($match = NULL, $match_operator = 'CONTAINS', $limit = 0) {
    $options = [];
    $bundles = $this->entityTypeManager->getBundleInfo('widget');
    $bundle_names = $this->getConfiguration()['target_bundles'] ?: array_keys($bundles);
    $current_domain = $this->domainNegotiator->getActiveDomain()->getDomainId();
    $query = $this->buildEntityQuery($match, $match_operator);

    if ($limit > 0) {
      $query->range(0, $limit);
    }

    $result = $query->execute();

    foreach ($bundle_names as $bundle) {
      if ($sub_widgets = $this->entityTypeManager->getStorage('widget')
        ->loadByProperties(['type' => $bundle, 'wid' => $result])) {
        foreach ($sub_widgets as $sub_widget) {
          $domain_values = $this->domainAccessManager->getAccessValues($sub_widget);
          $all_value = $this->domainAccessManager
            ->getAllValue($sub_widget);

          if ($all_value || in_array($current_domain, $domain_values)) {
            $options[$bundle][$sub_widget->id()] = Html::escape($this->entityTypeManager
              ->getTranslationFromContext($sub_widget)->label());
          }
        }
      }
    }
    return $options;
  }

}
